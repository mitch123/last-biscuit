
/* This program allows two players to play a game of last biscuit.
 *  They take turns taking as many biscuits as they want from one
 *  or an equal amount from both barrels.
 *  The player who takes the last biscuit wins the game. Level 3 attempted.
 */
import java.util.Scanner;

public class LastBiscuit {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int barrel1 = 6;
        int barrel2 = 8;
        int numberTaken;
        String whichBarrel; // "one", "two" or "both"
        boolean isPlayerOneTurn = false; // true if it is player 1's turn, false if player 2's
        String playerTurn; // "1" or "2"

        final String REMAINING_BARREL1 = "Biscuits Left - Barrel 1: ";
        final String REMAINING_BARREL2 = "Biscuits Left - Barrel 2: ";
        final String WHICH_BARREL = "From barrel1 (one), barrel2 (two), or both (both)? ";
        final String TAKEN_ERROR = "Enter an integer value, greater than zero, that is less than or equal to what's available in one of the barrels";
        final String BARREL_ERROR = "Enter a given barrel name which has enough biscuits that can be taken from it";

        do {

            // Starts turn if any biscuits are left and determines which player's turn it is
            if (barrel1 != 0 || barrel2 != 0) {
                isPlayerOneTurn = !isPlayerOneTurn;
                if (isPlayerOneTurn == true) {
                    playerTurn = "1";
                } else {
                    playerTurn = "2";
                }
                String takenByPlayer = "Biscuits taken by player " + playerTurn + ": ";

                System.out.println(REMAINING_BARREL1 + barrel1);
                System.out.println(REMAINING_BARREL2 + barrel2);

                /*
                 * ask player to choose how many biscuits they want, ask again if value is
                 * invalid (non-integer/outside range)
                 */
                System.out.print(takenByPlayer);
                do {
                    while (!in.hasNextInt()) {
                        System.out.println(TAKEN_ERROR);
                        System.out.println(REMAINING_BARREL1 + barrel1);
                        System.out.println(REMAINING_BARREL2 + barrel2);
                        System.out.print(takenByPlayer);
                        in.next();
                    }
                    numberTaken = in.nextInt();

                    if ((numberTaken > barrel1 && numberTaken > barrel2) || numberTaken <= 0) {
                        System.out.println(TAKEN_ERROR);
                        System.out.println(REMAINING_BARREL1 + barrel1);
                        System.out.println(REMAINING_BARREL2 + barrel2);
                        System.out.print(takenByPlayer);
                    }
                } while ((numberTaken > barrel1 && numberTaken > barrel2) || numberTaken <= 0);

                /*
                 * asks which barrel they want to take from, ask again if they don't input one
                 * of the expected strings
                 */
                System.out.print(WHICH_BARREL);
                while (!(in.hasNext("one") || in.hasNext("two") || in.hasNext("both"))) {
                    System.out.println(BARREL_ERROR);
                    System.out.println(REMAINING_BARREL1 + barrel1);
                    System.out.println(REMAINING_BARREL2 + barrel2);
                    System.out.println(takenByPlayer + numberTaken);
                    System.out.print(WHICH_BARREL);
                    in.next();
                }
                whichBarrel = in.next();

                /*
                 * takes amount of biscuits from player's chosen barrel(s) providing enough
                 * biscuits are left. If not, ask for a different barrel.
                 */
                boolean isValidBarrel = false;
                do {
                    if (whichBarrel.equals("one") && numberTaken <= barrel1) {
                        barrel1 -= numberTaken;
                        isValidBarrel = true;
                    } else if (whichBarrel.equals("two") && numberTaken <= barrel2) {
                        barrel2 -= numberTaken;
                        isValidBarrel = true;
                    } else if (whichBarrel.equals("both") && numberTaken <= barrel1 && numberTaken <= barrel2) {
                        barrel1 -= numberTaken;
                        barrel2 -= numberTaken;
                        isValidBarrel = true;
                    } else {
                        System.out.println(BARREL_ERROR);
                        System.out.println(REMAINING_BARREL1 + barrel1);
                        System.out.println(REMAINING_BARREL2 + barrel2);
                        System.out.println(takenByPlayer + numberTaken);
                        System.out.print(WHICH_BARREL);
                        while (!(in.hasNext("one") || in.hasNext("two") || in.hasNext("both"))) {
                            System.out.println(BARREL_ERROR);
                            System.out.println(REMAINING_BARREL1 + barrel1);
                            System.out.println(REMAINING_BARREL2 + barrel2);
                            System.out.println(takenByPlayer + numberTaken);
                            System.out.print(WHICH_BARREL);
                            in.next();
                        }
                        whichBarrel = in.next();
                    }
                } while (isValidBarrel == false);
            }

        } while (barrel1 > 0 || barrel2 > 0);

        /*
         * Print number of biscuits left in each barrel (should be zero in both) and the
         * winner of the game
         */
        System.out.println(REMAINING_BARREL1 + barrel1);
        System.out.println(REMAINING_BARREL2 + barrel2);

        if (isPlayerOneTurn == true) {
            System.out.println("Winner is player 1");
        } else {
            System.out.println("Winner is player 2");
        }
    }
}